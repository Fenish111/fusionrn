import { Platform } from 'react-native';

let baseUrl = '';

if (Platform.OS === 'ios') {
    baseUrl = 'http://fusion.test/api/v1/';
} else if (Platform.OS === 'android') {
    baseUrl = 'http://10.0.2.2/fusioninvoice/api/v1/';
} else {
    // Default base URL for other platforms
    baseUrl = 'http://default-base-url.com/api/v1/';
}

const makeApiUrl = (baseUrl, name) => {
    return baseUrl + name;
}

const api_login = makeApiUrl(baseUrl, 'login');
const api_clients = makeApiUrl(baseUrl, 'clients');
const api_client_store = makeApiUrl(baseUrl, 'clients/store');
const api_invoices = makeApiUrl(baseUrl, 'invoices');
const api_invoice_store = makeApiUrl(baseUrl, 'invoices/store');
const api_quotes = makeApiUrl(baseUrl, 'quotes');
const api_quotes_store = makeApiUrl(baseUrl, 'quotes/store');
const api_token = '1|49J7OYGzIka7h1sJD7qeYDRZdKCz7R2yK0fDlqMi3fc97966';

export  {
    api_token,
    api_login,
    api_clients,
    api_client_store,
    api_invoices,
    api_invoice_store,
    api_quotes,
    api_quotes_store
};
