import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, StatusBar, Alert } from 'react-native';
import { api_clients } from '../../../paths/_api_path';
import { request } from '../features/ClientSlice';
import { useDispatch, useSelector } from 'react-redux';
import { Actionsheet, Box, Center, Button, useDisclose, Icon, Text, VStack } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Octicons from 'react-native-vector-icons/Octicons';

const Index = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const clients = useSelector(state => state.client.items?.data?.data);
    const { isOpen, onOpen, onClose } = useDisclose();

    useEffect(() => {
        dispatch(request({ url: api_clients }));
    }, []);

    // const renderItem = ({ index, item }) => {

    //     return (
    //         <Box p="3" mb="1" bg="gray.100" borderRadius="md" shadow={1} flexDirection="row" justifyContent="space-between">
    //             <VStack>
    //                 <Text bold>{index + 1}.{item.id} {item.name}</Text>
    //                 <Text> {item.email}</Text>
    //                 <Text color={(item.active == 1) ? 'green.500' : 'red.500'}> {(item.active == 1) ? 'Active' : 'Inactive'}</Text>
    //             </VStack>
    //             <Center>
    //                 <Button
    //                     variant="outline"
    //                     size="sm"
    //                     p={1}
    //                     onPress={onOpen}
    //                     endIcon={<Icon as={MaterialIcons} name="arrow-drop-down" />}
    //                 >
    //                     Options
    //                 </Button>
    //                 <Actionsheet isOpen={isOpen} onClose={onClose} size="full">
    //                     <Actionsheet.Content>
    //                         <Box w="100%" px={4} justifyContent="center">
    //                             <Text
    //                                 fontSize="16"
    //                                 color="yellow.500"
    //                                 _dark={{ color: 'yellow.300' }}>
    //                                 Options
    //                             </Text>
    //                         </Box>
    //                         <Actionsheet.Item onPress={handleView} startIcon={<Icon as={MaterialIcons} size="5" name="remove-red-eye" />}>
    //                             <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>View</Text>
    //                         </Actionsheet.Item>
    //                         <Actionsheet.Item onPress={handleEdit} startIcon={<Icon as={MaterialIcons} size="5" name="edit" />}>
    //                             <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>Edit</Text>
    //                         </Actionsheet.Item>
    //                         <Actionsheet.Item onPress={handleCancel} startIcon={<Icon as={MaterialIcons} size="5" name="cancel" />}>
    //                             <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>Cancel</Text>
    //                         </Actionsheet.Item>
    //                         <Actionsheet.Item onPress={() => handleDelete(item.id)} startIcon={<Icon as={MaterialIcons} size="5" name="delete" color="red.400" />}>
    //                             <Text fontSize="15" color="red.500" _dark={{ color: 'red.300' }}>Delete</Text>
    //                         </Actionsheet.Item>
    //                     </Actionsheet.Content>
    //                 </Actionsheet>
    //             </Center>
    //         </Box>
    //     );
    // };

    const [actionsheetStates, setActionsheetStates] = useState([]);

    // Function to toggle the Actionsheet state for a specific item

    const toggleActionsheet = (index) => {
        setActionsheetStates(prevState => {
            const newState = [...prevState];
            newState[index] = !newState[index];
            return newState;
        });
    };

    const renderItem = ({ index, item }) => {
        const isOpen = actionsheetStates[index] || false;

        const handleView = () => {
            console.log(`View clicked for item with ID: ${item.id}`);
        };

        const handleEdit = () => {
            console.log(`Edit clicked for item with ID: ${item.id}`);
        };

        const handleCancel = () => {
            console.log(`Cancel clicked for item with ID: ${item.id}`);
        };

        const handleDelete = () => {
            Alert.alert('Delete invoice', `Are you sure you want to delete this invoice? ${item.id}`, [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: 'Delete',
                    style: 'destructive',
                    onPress: () => {
                        // Dispatch delete action
                        //dispatch(deleteInvoice(item.id));
                    }
                }
            ]);
        };

        return (
            <Box p="3" mb="1" bg="gray.100" borderRadius="md" shadow={1} flexDirection="row" justifyContent="space-between">
                <VStack>
                    <Text bold>{index + 1}.{item.id} {item.name}</Text>
                    <Text> {item.email}</Text>
                    <Text color={(item.active == 1) ? 'green.500' : 'red.500'}> {(item.active == 1) ? 'Active' : 'Inactive'}</Text>
                </VStack>
                <Center>
                    <Button
                        variant="outline"
                        size="sm"
                        p={1}
                        onPress={() => toggleActionsheet(index)} // Toggle Actionsheet for this item
                        endIcon={<Icon as={MaterialIcons} name="arrow-drop-down" />}
                    >
                        Options
                    </Button>
                    <Actionsheet isOpen={isOpen} onClose={() => toggleActionsheet(index)} size="full">
                        <Actionsheet.Content>
                            <Actionsheet.Item onPress={handleView} startIcon={<Icon as={MaterialIcons} size="5" name="remove-red-eye" />}>
                                <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>View</Text>
                            </Actionsheet.Item>
                            <Actionsheet.Item onPress={handleEdit} startIcon={<Icon as={MaterialIcons} size="5" name="edit" />}>
                                <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>Edit</Text>
                            </Actionsheet.Item>
                            <Actionsheet.Item onPress={handleCancel} startIcon={<Icon as={MaterialIcons} size="5" name="cancel" />}>
                                <Text fontSize="15" color="gray.500" _dark={{ color: 'gray.300' }}>Cancel</Text>
                            </Actionsheet.Item>
                            <Actionsheet.Item onPress={handleDelete} startIcon={<Icon as={MaterialIcons} size="5" name="delete" color="red.400" />}>
                                <Text fontSize="15" color="red.500" _dark={{ color: 'red.300' }}>Delete</Text>
                            </Actionsheet.Item>
                        </Actionsheet.Content>
                    </Actionsheet>
                </Center>
            </Box>
        );
    };

    useEffect(() => {
        // Initialize actionsheetStates array with false values for each item
        setActionsheetStates(new Array(clients.length).fill(false));
    }, [clients]); // Update actionsheetStates when clients change




    return (

        <View style={styles.container}>
            <View style={styles.buttonContainer}>
                <Button
                    variant="solid"
                    size="sm"
                    p={1}
                    onPress={() => navigation.navigate('createClient')}
                    leftIcon={
                        <Icon as={Octicons} name="plus" />
                    }>
                    Add
                </Button>

            </View>
            <FlatList
                data={clients}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                ListEmptyComponent={() => (
                    <Text style={styles.emptyListText}>No clients found</Text>
                )}
            />

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight || 0,
        paddingHorizontal: 10,
    },
    emptyListText: {
        alignSelf: 'center',
        marginTop: 50,
        fontSize: 16,
    },
    buttonContainer: {
        marginVertical: 20,
        alignItems: 'center',
    },
});

export default Index;
