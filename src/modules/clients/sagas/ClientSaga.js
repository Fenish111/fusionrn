import { take, put, call, fork } from 'redux-saga/effects';

import { request, success, failure } from '../features/ClientSlice';

import ApiHelper  from '../../../helper/api_helper'

function callGetRequest(url, data, headers) {
    console.log(url, data, headers);
    return ApiHelper.get(url, data, headers);
}

function* watchRequest() {
    console.log('|------SAGA------START------|');
    while (true) {
        const { payload } = yield take(request);
        console.log('client saga: payload');
        console.log(payload);
        try {

            let response;
            response = yield call(callGetRequest, payload.url, {});
            console.log('---------- RESPONSE ----------');

            console.log(response);
            yield put(success(response));

        } catch (ex) {
            yield put(failure(ex));
        }
    }
}

export default function* root() {
    yield fork(watchRequest);
}