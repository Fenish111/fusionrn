import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    items: [],
    isFetching: false,
    failure: false,
    errorMessage: undefined,
};

export const ClientSlice = createSlice({
    name: 'client',
    initialState,
    reducers: {
        request: state => {
            state.isFetching = true;
            console.log('client slice: request function');
        },
        success: (state, action) => {
            console.log('3Log SAGA Success');

            state.items = action.payload;
            state.isFetching = false;
            state.failure = false;
            state.errorMessage = undefined;
        },
        failure: (state, action) => {
            console.log('4Log SAGA Failure');
            console.log(action.payload);
            state.isFetching = false;
            state.failure = true;
            state.errorMessage = action.payload;
        },
    }
});
export const { request, success, failure } = ClientSlice.actions;
export default ClientSlice.reducer;
