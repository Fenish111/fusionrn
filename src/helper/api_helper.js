import { create } from 'apisauce';
import { useSelector } from 'react-redux';
import { baseUrl } from '../paths/_api_path'
const api = create({
    baseURL: baseUrl,
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Cache-Control': 'no-cache',
        // 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
        // 'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Origin': 'http://fusion.test'
    },
});
const token = '1|49J7OYGzIka7h1sJD7qeYDRZdKCz7R2yK0fDlqMi3fc97966';
class api_helper {


    accessToken = token;

    getUpdatedHeader = headers => {
        if (this.accessToken) {
            return { ...headers, 'Authorization': 'Bearer ' + this.accessToken };
        } else {
            return headers;
        }
    };

    get = async (url, data, headers) => {
        const updatedHeader = this.getUpdatedHeader(headers);
        const response = await api.get(url, data, { headers: updatedHeader });
        return new Promise((resolve, reject) => {
            this.handleErrors(resolve, reject, response);
        });
    };

    post = async (url, data, headers) => {
        const updatedHeader = this.getUpdatedHeader(headers);

        console.log(this.accessToken);
        console.log('updatedHeader');
        console.log(updatedHeader);
        const response = await api.post(url, data, { headers: updatedHeader });

        return new Promise((resolve, reject) => {
            this.handleErrors(resolve, reject, response);
        });
    };

    put = async () => { };

    delete = () => { };

    login = async (url, data) => {
        const response = await api.post(url, data);
        return new Promise((resolve, reject) => {
            this.handleErrors(resolve, reject, response);
        });
    };

    handleErrors = (resolve, reject, response) => {
        console.log('Handle Errors Function Called');
        console.log(response);
        if (response.error) {
            if (response.error.code === 'NETWORK_ISSUE') {
                reject('You have network problem');
            }
            if (response.error.code === 'BANDWIDTH_ISSUE') {
                reject('You have network problem');
            }
        }
        // else if(response.problem){
        //   if (response.problem === 'NETWORK_ERROR') {
        //     reject('You have network problem');
        //   }
        // }
        else {
            resolve(response);
        }
    };

}

export default new api_helper();
