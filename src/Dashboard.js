import { Badge, Box, HStack, Spacer, VStack, Flex, Text, Stack } from "native-base";
import React from "react";
import { Pressable, View } from "react-native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Index from "./modules/clients/component/Index";
import Create from "./modules/clients/component/Create";
import { SafeAreaView } from "react-native-safe-area-context";

const StackRoute = createNativeStackNavigator();

function Example1() {
    return <Box alignItems="center" bg="primary.400" rounded="sm" shadow={"3"}>
        <Pressable onPress={() => console.log("I'm Pressed")} rounded="8" overflow="hidden" borderWidth="1" borderColor="coolGray.300" maxW="96" shadow="3" bg="coolGray.100" p="5">
            <Box>
                <HStack alignItems="center">
                    <Badge colorScheme="darkBlue" _text={{
                        color: "white"
                    }} variant="solid" rounded="4">
                        Business
                    </Badge>
                    <Spacer />
                    <Text fontSize={10} color="coolGray.800">
                        1 month ago
                    </Text>
                </HStack>
                <Text color="coolGray.800" mt="3" fontWeight="medium" fontSize="xl">
                    Marketing License
                </Text>
                <Text mt="2" fontSize="sm" color="coolGray.700">
                    Unlock powerfull time-saving tools for creating email delivery and
                    collecting marketing data
                </Text>
                <Flex>
                    <Text mt="2" fontSize={12} fontWeight="medium" color="darkBlue.600">
                        Read More
                    </Text>
                </Flex>
            </Box>
        </Pressable>
    </Box>;
}

function Example() {
    return <Box alignItems="center">
        <Pressable maxW="96">
            {({
                isHovered,
                isFocused,
                isPressed
            }) => {
                return <Box bg={isPressed ? "coolGray.200" : isHovered ? "coolGray.200" : "coolGray.100"} style={{
                    transform: [{
                        scale: isPressed ? 0.96 : 1
                    }]
                }} p="5" rounded="8" shadow={3} borderWidth="1" borderColor="coolGray.300">
                    <HStack alignItems="center">
                        <Badge colorScheme="darkBlue" _text={{
                            color: "white"
                        }} variant="solid" rounded="4">
                            Business
                        </Badge>
                        <Spacer />
                        <Text fontSize={10} color="coolGray.800">
                            1 month ago
                        </Text>
                    </HStack>
                    <Text color="coolGray.800" mt="3" fontWeight="medium" fontSize="xl">
                        Marketing License
                    </Text>
                    <Text mt="2" fontSize="sm" color="coolGray.700">
                        Unlock powerfull time-saving tools for creating email delivery
                        and collecting marketing data
                    </Text>
                    <Flex>
                        {isFocused ? <Text mt="2" fontSize={12} fontWeight="medium" textDecorationLine="underline" color="darkBlue.600" alignSelf="flex-start">
                            Read More
                        </Text> : <Text mt="2" fontSize={12} fontWeight="medium" color="darkBlue.600">
                            Read More
                        </Text>}
                    </Flex>
                </Box>;
            }}
        </Pressable>
    </Box>;
}


const Dashboard = (props) => {
    return (
        <SafeAreaView style={{flex:1, backgroundColor:'red'}}>
        <View>
            <Text> Dashboard</Text>
            <Stack>
                <MyStack />
            </Stack>
        </View>
        </SafeAreaView>
    );
}

export default Dashboard;
