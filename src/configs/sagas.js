import { fork } from 'redux-saga/effects';

import ClientSaga from '../modules/clients/sagas/ClientSaga';

export default function* rootSaga() {
    yield fork(ClientSaga);
}