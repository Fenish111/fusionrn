import React, { useState, useEffect } from 'react';
import 'react-native-gesture-handler';
import { SafeAreaView, View } from 'react-native';
import { store, persistor } from './src/configs/store';
import { PersistGate } from 'redux-persist/integration/react';

import { LogBox } from 'react-native';

import {
  NativeBaseProvider,
  Box,
  Link,
  Text,
  VStack,
  Switch,
  useColorMode,
  FormControl,
  Input,
  Stack,
  WarningOutlineIcon,
  IconButton,
  CloseIcon,
  Alert,
} from 'native-base';
import { NavigationContainer } from '@react-navigation/native';


import theme from './theme/index';
import Dashboard from './src/Dashboard';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Index from './src/modules/clients/component/Index';
import Create from './src/modules/clients/component/Create';
import { Provider } from 'react-redux';

function ToggleDarkMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Stack direction="row" alignItems="center" space={2}>
      <Text>Dark</Text>
      <Switch
        isChecked={colorMode === 'light' ? true : false}
        onToggle={toggleColorMode}
        aria-label={
          colorMode === 'light' ? 'switch to dark mode' : 'switch to light mode'
        }
      />
      <Text>Light</Text>
    </Stack>
  );
}

const StackRoute = createNativeStackNavigator();

function MyStack() {
  return (
    <StackRoute.Navigator>
      <StackRoute.Screen name="Home" component={Index} />
      <StackRoute.Screen name="createClient" component={Create} />
    </StackRoute.Navigator>
  );
}

export default function App() {
  LogBox.ignoreLogs(['Warning: ...']);

  // Ignore all log notifications:
  LogBox.ignoreAllLogs();


  return (
    <Provider store={store}>
      {/* <PersistGate loading={null} persistor={persistor} onBeforeLift={() => {
        console.log('store ios lodded');
      }}> */}

        <NativeBaseProvider >
          <NavigationContainer>
            <SafeAreaView flex={1}>
              <MyStack />
            </SafeAreaView>
          </NavigationContainer>
        </NativeBaseProvider>
      {/* </PersistGate> */}
    </Provider>
  );
}
